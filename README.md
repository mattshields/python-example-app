# Python Example App

This is an example project that uses Python and Flask to display a Hello World page.  It also let's you pass in environment variables.

# Quick reference

-	**Maintained by**:  
	Matt Shields - [GitLab](https://gitlab.com/mattshields) [GitHub](https://github.com/mattboston)

-   **Docker Hub**:
    [Python Example App](https://hub.docker.com/r/mattshields5/python-example-app)

-	**Where to file issues**:  
	[https://gitlab.com/mattshields5/python-example-app](https://gitlab.com/mattshields/python-example-app)

# How to use this image

```console
$ docker pull mattshields5/python-example-app:latest
$ docker run --name my-app -p 8000:8000 -e db_server="{db server fqdn}" -e db_username="{db username}" -e db_password="{db password}" mattshields5/python-example-app
```

Then you can hit `http://localhost:8000` or `http://host-ip:8000` in your browser.

# License

This project is licensed under the [GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html) license.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).  Use of this project is with no warranty or support, but feel free to open an issue if you find a bug.  As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
