from flask import Flask, render_template
import os

app = Flask(__name__, template_folder='./templates', static_folder='./static')

@app.route('/')
def index():
    db_info = {
        'db_server': os.environ.get('db_server'),
        'db_username': os.environ.get('db_username'),
        'db_password': os.environ.get('db_password'),
    }

    return render_template('index.html', db_info=db_info)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
