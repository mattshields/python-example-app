FROM python:3-alpine

ENV db_server=""
ENV db_username=""
ENV db_password=""
ENV PYTHONPATH=/app

WORKDIR /app

COPY . /

RUN pip install --upgrade pip && \
pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD [ "python", "app.py" ]

# Metadata
LABEL org.opencontainers.image.vendor="Matt Shields" \
	org.opencontainers.image.url="https://gitlab.com/mattshields/python-example-app" \
	org.opencontainers.image.title="Python Example App" \
	org.opencontainers.image.description="A sample python and flask docker app" \
	org.opencontainers.image.version="v1.0.1" \
	org.opencontainers.image.documentation="https://gitlab.com/mattshields/python-example-app"
    